﻿(function ($) {

    if (typeof Tempus === "undefined" || Tempus == null) Tempus = {};
    if (typeof Tempus.Forms === "undefined" || Tempus.Forms == null) Tempus.Forms = {};
    if (typeof Tempus.Forms.Controls === "undefined" || Tempus.Forms.Controls == null) Tempus.Forms.Controls = {};
    if (typeof Tempus.Forms.Controls.SPIRIT === "undefined" || Tempus.Forms.Controls.SPIRIT == null) Tempus.Forms.Controls.SPIRIT = {};

    Tempus.Forms.Controls.SPIRIT.Navigation = {
        //internal method used to get a handle on the control instance
        _getInstance: function (id) {
            var control = jQuery('#' + id);
            if (control.length == 0) {
                throw 'Navigation \'' + id + '\' not found';
            } else {
                return control[0];
            }
        },

        getProperty: function (objInfo) {
            if (objInfo.property.toLowerCase() == "value") {
                return SourceCodeANZ.Forms.Controls.Menu.getValue(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isvisible") {
               Tempus.Forms.Controls.SPIRIT.Navigation.setIsVisible(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isenabled") {
               Tempus.Forms.Controls.SPIRIT.Navigation.setIsEnabled(objInfo);
            }
            else {
                var jqData = $('#' + objInfo.CurrentControlId).data('options');
                return jqData[objInfo.property.toLowerCase()];
            }
        },

        _setPropertyValue: function (controlId, fieldName, value) {
            var jqData = $('#' + controlId).data('options');
            jqData[fieldName] = value;
            $('#' + controlId).data('options', jqData);
        },

        setProperty: function (objInfo) {
            switch (objInfo.property.toLowerCase()) {
                case "isvisible":
                   Tempus.Forms.Controls.SPIRIT.Navigation.setIsVisible(objInfo);
                    break;

                case "isenabled":
                   Tempus.Forms.Controls.SPIRIT.Navigation.setIsEnabled(objInfo);
                    break;

                default:
                    {
                       Tempus.Forms.Controls.SPIRIT.Navigation._setPropertyValue(objInfo.CurrentControlId, objInfo.property.toLowerCase(), objInfo.Value);
                        $('#' + objInfo.CurrentControlId).find('#' + objInfo.property.toLowerCase()).val(objInfo.Value);
                    }
            }
        },

        //helper method to set visibility
        setIsVisible: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isVisible = value;
            var displayValue = (value === false) ? "none" : "block";
            var instance =Tempus.Forms.Controls.SPIRIT.Navigation._getInstance(objInfo.CurrentControlId);
            instance.style.display = displayValue;
        },

        //helper method to set control "enabled" state
        setIsEnabled: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isEnabled = value;
            var instance =Tempus.Forms.Controls.SPIRIT.Navigation._getInstance(objInfo.CurrentControlId);
            instance.readOnly = !value;
        },


        executeMethod: function (objInfo) {
            switch (objInfo.methodName) {
                case "FadeIn":
                    $('#' + objInfo.CurrentControlId).closest('.row').show();
                    $('#' + objInfo.CurrentControlId).closest('.formcontrol').show();
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeIn(fadeTime);
                    break;
                case "FadeOut":
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeOut(fadeTime);
                    $('#' + objInfo.CurrentControlId).closest('.formcontrol').hide();
                    $('#' + objInfo.CurrentControlId).closest('.row').hide();
                    break;

                case "SetNavigationItems":
                    var namesOfNav = objInfo.methodParameters.nameofitems;      
                    var navNameArray = namesOfNav.split("|");        
                    var numOfNavItems = navNameArray.length;
                    var percentageOfWidth = 12 / numOfNavItems;
                    for (var i = 0; i < numOfNavItems; i++) {
                        var labelInput = $('<label>', {
                            id: 'nav-item-' + (i + 1) + '-label',
                            text: navNameArray[i],
                            class: 'label nav-item-label'
                        });

                        var divInput = $('<div>', {
                            class: 'nav-item-container'
                        });

                        var liInput = $('<li>', {
                            id: 'nav-item-' + (i + 1),
                            class: 'nav-item w3-col' + ' s' + percentageOfWidth + ' m' + percentageOfWidth + ' l' + percentageOfWidth
                        })

                        var containerItem = liInput.append(divInput.append(labelInput));
                        $('#nav-bar-list').append(containerItem);
                    }
                    break;

                case "SetNavigationStage":
                    var stageNum = objInfo.methodParameters.stagenumber;
                    var stageNum2 = parseInt(stageNum) + 1; 
                    $('#nav-item-' + stageNum).addClass('active');
                    $('#nav-item-' + (stageNum2)).removeClass('active');
                    break;
            }
        },

        _attachEventHandler: function ($this) {
            var controlid = $this.attr('id')

            $this.on('click', function () {
                raiseEvent(controlid, 'Control', 'OnClick')
            });

        }
    }
})(jQuery)


$(document).ready(function () {

    $('.SFC.Tempus-Forms-Controls-SPIRIT-Navigation').each(function (i, element) {
        var $this = $(element)

        var str = 
'            <div class="navigation-bar">' +
'                <div class="w3-container navigation-bar-container">' +
'                    <div class="w3-row" style="margin-top:0px;">' +
'                        <ol id="nav-bar-list">' +
'                        </ol>' +
'                    </div>' +
'                </div>' +
'            </div> '

        var html = $.parseHTML(str)
        $(html).appendTo($this)

       Tempus.Forms.Controls.SPIRIT.Navigation._attachEventHandler($this)
        //initialise the data array
        $this.data("options", {});

    });

});