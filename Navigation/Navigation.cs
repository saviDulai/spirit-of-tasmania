﻿using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("Tempus.Forms.Controls.SPIRIT.Global.Style.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.SPIRIT.Global.w3.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Tempus.Forms.Controls.SPIRIT.Navigation.Navigation.js", "text/javascript", PerformSubstitution = true)]

namespace Tempus.Forms.Controls.SPIRIT
{
    [ControlTypeDefinition("Tempus.Forms.Controls.SPIRIT.Navigation.Navigation.xml")]
    [ClientCss("Tempus.Forms.Controls.SPIRIT.Global.Style.css")]
    [ClientScript("Tempus.Forms.Controls.SPIRIT.Navigation.Navigation.js")]
    [ClientCss("Tempus.Forms.Controls.SPIRIT.Global.w3.css")]

    class Navigation : BaseControl
    {
        #region properties
        public bool IsVisible
        {
            get
            {
                return this.GetOption<bool>("isvisible", true);
            }
            set
            {
                this.SetOption<bool>("isvisible", value, true);
            }
        }

         public bool IsEnabled
        {
            get
            {
                return this.GetOption<bool>("isenabled", true);
            }
            set
            {
                this.SetOption<bool>("isenabled", value, true);
            }
        }

        #endregion
        //constructor
        public Navigation(): base("div") 
        {

        }

        protected override void CreateChildControls()
        {
            this.Attributes.CssStyle.Add(HtmlTextWriterStyle.Width, "100%");
            if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime)
            {
                Label lbl = new Label();
                lbl.Text = "Navigation";
                lbl.Style["padding-left"] = "2px";
                
                this.Controls.Add(lbl);
             }
            else if (this.State == SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime)
            {

            }
        }

    }
}