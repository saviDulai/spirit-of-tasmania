﻿using System;
using System.Web;
using System.Text;
using SourceCode.Forms.Controls.Web.SDK.Attributes;
using System.Net;
using SourceCode.SmartObjects.Client;
using SourceCode.Forms.Controls.Web.SDK.Utilities;
using SourceCode.Data.SmartObjectsClient;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tempus.Forms.Controls.SPIRIT
{
    [ClientAjaxHandler("DSGAjaxHandler.handler")]
    public class AutoCompleteAjaxHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return false; }
        }
        

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // client passes server an operation id (oid) to determine which method to call
                string oid = context.Request["oid"];               
                        // WebProxy proxy = new WebProxy("mywebproxyserver.com");          //set a legit proxy here and change useProxy = true
                //WebProxy proxy = new WebProxy("http://172.19.254.10:8080", true);

                // GUID probably is overkill here but stops any random guessing of the oid
                
                

            }
            catch (Exception ex)
            {
                // https://stackoverflow.com/questions/10732644/best-practice-to-return-errors-in-asp-net-web-api
                // [‎28/‎08/‎2018 4:05 PM] Sarbraj Dulai: 
                //HttpResponseMessage response =
                //this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "your message");
                //throw new HttpResponseException(response);
                // need to thorw proper

                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                sb.Append("{");
                sb.AppendFormat("\"text\":\"{0}\"", ex.Message);
                sb.Append("},");
                if (sb.Length > 2)
                    sb.Remove(sb.Length - 1, 1);
                sb.Append("]");
                context.Response.Write(sb.ToString());
            }
        }

        #endregion
        

    }

}
