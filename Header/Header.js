﻿(function ($) {

    if (typeof Tempus === "undefined" || Tempus == null) Tempus = {};
    if (typeof Tempus.Forms === "undefined" || Tempus.Forms == null) Tempus.Forms = {};
    if (typeof Tempus.Forms.Controls === "undefined" || Tempus.Forms.Controls == null) Tempus.Forms.Controls = {};
    if (typeof Tempus.Forms.Controls.SPIRIT === "undefined" || Tempus.Forms.Controls.SPIRIT == null) Tempus.Forms.Controls.SPIRIT = {};

   Tempus.Forms.Controls.SPIRIT.Header = {
        //internal method used to get a handle on the control instance
        _getInstance: function (id) {
            var control = jQuery('#' + id);
            if (control.length == 0) {
                throw 'Header \'' + id + '\' not found';
            } else {
                return control[0];
            }
        },

        getProperty: function (objInfo) {
            if (objInfo.property.toLowerCase() == "value") {
                return SourceCodeANZ.Forms.Controls.Menu.getValue(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isvisible") {
               Tempus.Forms.Controls.SPIRIT.Header.setIsVisible(objInfo);
            }
            else if (objInfo.property.toLowerCase() == "isenabled") {
               Tempus.Forms.Controls.SPIRIT.Header.setIsEnabled(objInfo);
            }
            else {
                var jqData = $('#' + objInfo.CurrentControlId).data('options');
                return jqData[objInfo.property.toLowerCase()];
            }
        },

        _setPropertyValue: function (controlId, fieldName, value) {
            var jqData = $('#' + controlId).data('options');
            jqData[fieldName] = value;
            $('#' + controlId).data('options', jqData);
        },

        setProperty: function (objInfo) {
            switch (objInfo.property.toLowerCase()) {
                case "isvisible":
                   Tempus.Forms.Controls.SPIRIT.Header.setIsVisible(objInfo);
                    break;

                case "isenabled":
                   Tempus.Forms.Controls.SPIRIT.Header.setIsEnabled(objInfo);
                    break;

                default:
                    {
                       Tempus.Forms.Controls.SPIRIT.Header._setPropertyValue(objInfo.CurrentControlId, objInfo.property.toLowerCase(), objInfo.Value);
                        $('#' + objInfo.CurrentControlId).find('#' + objInfo.property.toLowerCase()).val(objInfo.Value);
                    }
            }
        },

        //helper method to set visibility
        setIsVisible: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isVisible = value;
            var displayValue = (value === false) ? "none" : "block";
            var instance =Tempus.Forms.Controls.SPIRIT.Header._getInstance(objInfo.CurrentControlId);
            instance.style.display = displayValue;
        },

        //helper method to set control "enabled" state
        setIsEnabled: function (objInfo) {
            value = (objInfo.Value === true || objInfo.Value == 'true');
            this._isEnabled = value;
            var instance =Tempus.Forms.Controls.SPIRIT.Header._getInstance(objInfo.CurrentControlId);
            instance.readOnly = !value;
        },

        executeMethod: function (objInfo) {
            switch (objInfo.methodName) {
                case "FadeIn":
                    $('#' + objInfo.CurrentControlId).closest('.row').show();
                    $('#' + objInfo.CurrentControlId).closest('.formcontrol').show();
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeIn(fadeTime);
                    break;
                case "FadeOut":
                    var fadeTime = 1000;
                    if (objInfo.methodParameters.time)
                        fadeTime = parseInt(objInfo.methodParameters.time);
                    $('#' + objInfo.CurrentControlId).fadeOut(fadeTime);
                    $('#' + objInfo.CurrentControlId).closest('.formcontrol').hide();
                    $('#' + objInfo.CurrentControlId).closest('.row').hide();
                    break;
                case "ScollToTop":
                    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                    document.documentElement.scrollTop = 0; // For IE and Firefox
                    break;
                case "UpdateFormTitleAndUser":
                    var title = objInfo.methodParameters.title;
                    var user = objInfo.methodParameters.currentuser;
                    if (user) {
                        $('#logged-in-user-container').show();
                        $('#current-user').text(user);
                    }
                    $('#form-title').text(title);
                    break;                
            }
        },

        _attachEventHandler: function ($this) {
            var controlid = $this.attr('id')

            $this.on('click', function () {
                raiseEvent(controlid, 'Control', 'OnClick')
            });

            //$this.find('#FIELDNAME').on('change', function () {
            //    var updatedValue = $this.find('#FIELDNAME').val();
            //    Tempus.Forms.Controls.MyStateTaxInformation._setPropertyValue(controlid, 'FIELDNAME', updatedValue);
            //});
        }
    }
})(jQuery)

$(document).ready(function () {

    $('.SFC.Tempus-Forms-Controls-SPIRIT-Header').each(function (i, element) {
        var $this = $(element)

        var str = 
'            <header id="SPIRIT-header"> ' +
'                <div class="w3-container" id="SPIRIT-header-container"> ' +
'                   <div class="w3-row" style="margin-bottom:0px;"> ' +
'                       <div class="w3-col s12 m10 l10"> ' +
'                           <div id="header-logo" class="logo form-info-section"> ' +
'                               <a href="https://intranet.spiritoftasmania.com.au/"> ' +
'                                   <img src="https://www.spiritoftasmania.com.au/media/713167/logored.png" alt="Spirits Logo" height="45" style="padding-right: 20px;"> ' +
'                               </a>' +
'                           </div> ' +
'                           <div id="form-info-container" class="form-info-section"> ' +
'                               <h2 id="form-title"></h2> ' +
'                           </div> ' +
'                       </div> ' +
'                       <div class="w3-col s12 m2 l2">' +
'                           <div id="logged-in-user-container" style="display:none;">' +
'                               <p><span class="usericon"></span> <span id="current-user"></span></p>' +
'                           </div>' +
'                       </div> ' +
'                   </div> ' +
'               </div> ' +
'               <hr style="margin-top: 0px;"> ' +
'           </header>' 

        var html = $.parseHTML(str)
        $(html).appendTo($this)

       Tempus.Forms.Controls.SPIRIT.Header._attachEventHandler($this)
        //initialise the data array
        $this.data("options", {});
    });
});